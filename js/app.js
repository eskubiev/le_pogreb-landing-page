// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

//Smooth scrolling when clicking a link
var $root = $('html, body');
$('a').click(function() {
	$root.animate({
		scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
	return false;
});

//Slider
$(document).ready(function(){
	$('.image-slider').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 4000,
	});
});