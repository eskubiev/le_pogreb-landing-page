<?php

//Config
require_once ("inc/config.php");

//Form sending script
include_once (ROOT_PATH . "inc/send.php");

?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="Установка эксклюзивных винных погребов">
		<title>Le Pogreb | Установка эксклюзивных винных погребов</title>
		<link rel="author" href="humans.txt" />
		<link rel="stylesheet" href="css/app.css" />
		<!-- <script src="js/google-analytics.js"></script> -->
		<script src="js/modernizr-min.js"></script>
	</head>
	<body>
		
		<!-- navigation -->
		<nav class="top-bar" data-topbar>
			<ul class="title-area">
				<li class="name"></li>
				<li class="toggle-topbar"><a href="#">Меню / Телефон</a></li>
			</ul>
		
			<section class="top-bar-section">

				<!-- Left Nav Section -->
				<ul class="left">
					<li><a href="#unique-features">Уникальность погребов</a></li>
					<li class="divider"></li>
					<li><a href="#products">Наша продукция</a></li>
					<li class="divider"></li>
					<li><a href="#advantages">Почему выбирают погреба</a></li>
					<li class="divider"></li>
					<li><a href="#installation">Как мы работаем</a></li>
				</ul>

				<!-- Right Nav Section -->
				<ul class="right">
					<li><a href="tel:+74959201420">+7 (495) 920-14-20</a></li>
				</ul>

			</section>
		</nav><!-- end navigation -->
		
		<!-- main image -->
		<div id="main-image">
			<div class="padded-container">
				<div class="row">
					<div class="small-12 medium-4 columns">
						<div id="logo">
							<h1>Le Pogreb</h1>
							<p>Установка эксклюзивных</p>
							<p>винных погребов</p>
							<p>по всей России</p>
						</div>
					</div>
					<div class="medium-4 columns">
						<!-- <form name="form-1" method="post" action="<?php echo BASE_URL; ?>" data-abide> -->
						<form name="form-1" method="post" action="." data-abide>
							<?php include(ROOT_PATH . "inc/form.php"); ?>
							<div class="row">
								<div class="small-12 columns text-center">
									<button class="button alert radius" type="submit" name="form-1-submit"><span>Заказать звонок</span><br>Наш менеджер перезвонит вам</button>
								</div>
							</div>
						</form>
					</div>
				</div><!-- end row -->
			</div><!-- end padded-container -->
		</div><!-- main image -->
		
		<!-- main features -->
		<div id="main-features">
			<div class="row">
				<div class="medium-4 columns">
					<i class="icon-lilly"></i>
					<h4>Более 30 000 проектов</h4>
					<p>успешно реализованных в Европе</p>
				</div>
				<div class="medium-4 columns">
					<i class="icon-prize"></i>
					<h4>Сертификат качества</h4>
					<p>SOCOTEC, Франция</p>
				</div>
				<div class="medium-4 columns">
					<i class="icon-money"></i>
					<h4>Повышение стоимости дома</h4>
					<p>после установки погреба</p>
				</div>
			</div>
			
			<div class="row">
				<div class="medium-4 columns">
					<i class="icon-temple"></i>
					<h4>Возможность установки</h4>
					<p>в уже построенном доме</p>
				</div>
				<div class="medium-4 columns">
					<i class="icon-guarantee"></i>
					<h4>Гарантия 10 лет</h4>
					<p>от производителя</p>
				</div>
				<div class="medium-4 columns">
					<i class="icon-man"></i>
					<h4>Отсутствие аналогов</h4>
				</div>
			</div>
		</div><!-- end main features -->
		
				
		<!-- unique features -->
		<div class="padded-container">
			<div id="unique-features">
					
				<hr class="top-line">
				
				<div class="row">
					<div class="small-12 columns text-center">
						<h2>Уникальность погребов</h2>
					</div>
				</div>

				<hr class="underline">
				
				<div class="row">

					<div class="medium-4 columns">
						<img src="img/feature1-terms.jpg" alt="Винный погреб в комнате">
						<div class="row">
							<div class="large-2 columns">
								<i class="icon-time"></i>
							</div>
							<div class="large-9 columns feature-description">
								<h3>Сроки</h3>
								<p>За удивительно короткие сроки от 15 до 25 дней мы установим Спиральный винный погреб в любом месте вашего дома: в гостинной, кухне, кабинете, библиотеке и даже в гараже, как на стадии строительства, так и в уже построенном здании.</p>
							</div>
						</div>
					</div>

					<div class="medium-4 columns">
						<img src="img/feature2-form.jpg" alt="Форма винного погреба">
						<div class="row">
							<div class="large-2 columns">
								<div class="row">
									<div class="small-3 large-12 columns">
										<i class="icon-ronde"></i>
									</div>
								
									<div class="small-3 small-pull-6 medium-pull-5 large-12 large-pull-0 columns">
										<i class="icon-ovale"></i>
									</div>
								</div>
							</div>
							<div class="large-9 columns feature-description">
								<h3>Форма</h3>
								<p>Погреба LePogreb имеют круглую или овальную форму для оптимального распределения внешнего давления и обладают колоссальной вместительностью от 800 до 3500 бутылок в зависимости от модели.</p>
							</div>
						</div>
					</div>

					<div class="medium-4 columns">
						<img src="img/feature3-storage.jpg" alt="Полки винного погреба">
						<div class="row">
							<div class="large-2 columns">
								<i class="icon-year"></i>
							</div>
							<div class="large-10 columns feature-description">
								<h3>Хранение</h3>
								<p>Оптимальные условия для хранения вина круглый год. В отличие от винной комнаты, где требуется отдельное помещение, стеллажи и климатическая система, вы получаете необходимые условия 3 в 1: экономичный, практичный, экологически чистый, из натуральных материалов личный винный погреб.</p>
							</div>
						</div>
					</div>
				</div>
				
				<!-- call to action -->
				<div class="row">
					<div class="medium-7 columns">
						<div class="attention">
							<div class="row">
								<div class="small-2 columns text-center">
									<i class="icon-attention"></i>
								</div>
								<div class="small-10 columns">
									<p>По вашему желанию мы можем разработать индивидуальный дизайн-проект вашего спирального винного погреба с 3D-визуализацией любой сложности.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="medium-5 columns">
						<div class="form-wrapper">
							<!-- <form name="form-2" method="post" action="<?php echo BASE_URL; ?>" data-abide> -->
							<form name="form-2" method="post" action="." data-abide>
								<?php include(ROOT_PATH . "inc/form.php"); ?>
								<div class="row">
									<div class="small-12 columns text-center">
										<button class="button alert radius" type="submit" name="form-2-submit"><span>Заказать звонок</span><br>Наш менеджер перезвонит вам</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div><!-- end call to action -->
			</div><!-- end unique features -->
		</div><!-- end padded-container -->
		
		
		<!-- products -->
		<div class="padded-container">
			<div id="products">
				
				<hr class="top-line">
				
				<div class="row">
					<div class="small-12 columns text-center">
						<h2>Наша продукция</h2>
					</div>
				</div>

				<hr class="underline">
				
				<div class="row">
					<div class="medium-5 large-4 large-push-4 columns">
						<img id="product-img-1" src="img/product1-ronde.jpg" alt="Le Pogreb Ronde">
					</div>
					<div id="product-1-description" class="medium-7 large-4 large-pull-8 columns">
						<i class="icon-ronde"></i>
						<h3>Ronde</h3>
						<div class="specification">
							<p><span>Диаметр:</span> от 2м</p>
							<p><span>Глубина:</span> от 2м до 3м</p>
							<p><span>Вместимость:</span> от 800 до 1800 бутылок</p>
						</div>
						<p>самая маленькая модель с вместимостью от 800 бутылок, будет разработана по индивидуальному проекту специально для Вас - любителя вина и ценителя уюта.</p>
						<p>* Не требует никакого потребления энергии для поддержания необходимой температуры.</p>
					</div>
				</div>
				
				<div class="row">
					<div class="medium-5 large-5 large-push-3 columns">
						<img id="product-img-2" src="img/product2-maxi-ronde.jpg" alt="Le Pogreb Maxi Ronde">
					</div>
					<div id="product-2-description" class="medium-7 large-4 columns">
						<i class="icon-ronde"></i>
						<h3>Maxi Ronde</h3>
						<div class="specification">
							<p><span>Диаметр:</span> от 2м</p>
							<p><span>Глубина:</span> от 2м до 3м</p>
							<p><span>Вместимость:</span> от 1400 до 3000 бутылок</p>
						</div>
						<p>модель круглой формы, вместимость от 1400 до 3000 бутылок. Погреб для истинных ценителей вина и начинающих коллекционеров.</p>
						<p>* Повышение стоимости Вашего дома после установки винного погреба Le Pogreb.</p>
					</div>
				</div>
				
				<div class="row">
					<div class="medium-5 large-push-4 columns">
						<img id="product-img-3" src="img/product3-ovale.jpg" alt="Le Pogreb Ovale">
					</div>
					<div id="product-3-description" class="medium-7 large-4 large-pull-8 columns">
						<i class="icon-ovale"></i>
						<h3>Ovale</h3>
						<div class="specification">
							<p><span>Диаметр:</span> от 2м</p>
							<p><span>Глубина:</span> от 2м до 3м</p>
							<p><span>Вместимость:</span> от 800 до 1800 бутылок</p>
						</div>
						<p>погреб в форме овала, способный вместить до 1800 бутылок. Огромное пространство внутри погреба можно оформить в разных стилях.</p>
						<p>*Дополнительное пространство, удовлетворяющее всем желаниям: хранение вина, продуктов или громоздких предметов, фотомастерской и др.</p>
					</div>
				</div>
				
				<div class="row">
					<div class="medium-5 large-push-3 columns">
						<img id="product-img-4" src="img/product4-maxi-ovale.jpg" alt="Le Pogreb Maxi Ovale">
					</div>
					<div id="product-4-description" class="medium-7 large-4 columns">
						<i class="icon-ovale"></i>
						<h3>Maxi Ovale</h3>
						<div class="specification">
							<p><span>Диаметр:</span> от 3м</p>
							<p><span>Глубина:</span> от 2,25м до 2,75м</p>
							<p><span>Вместимость:</span> от 1400 до 3000 бутылок</p>
						</div>
						<p>это винное хранилище вместимостью от 1400 до 3500 бутылок. Идеальный вариант для настоящих коллекционеров, позволит организовать дегустационную комнату с полным погружением в философию вина.</p>
						<p>* Идеальные условия хранения вина благодаря совокупности требуемых качеств: спокойствие, темнота, благоприятная температура и влажность.</p>
					</div>
				</div>
			</div><!-- end products -->
		</div><!-- end padded-container -->
			
		<!-- call to action -->
		<div class="row">
			<div class="medium-7 medium-push-5 columns">
				<div class="attention">
					<div class="row">
						<div class="small-2 columns text-center">
							<i class="icon-attention"></i>
						</div>
						<div class="small-10 columns">
							<p>Наши профессионалы исполнят погреб любой сложности:</p>
							<p>погреба Le Pogreb будут изготовлены исключительно под Вас с учетом Ваших личных пожеланий и винных пристрастий.</p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="medium-5 medium-pull-7 columns">
				<div class="form-wrapper">
					<!-- <form name="form-3" method="post" action="<?php echo BASE_URL; ?>" data-abide> -->
					<form name="form-3" method="post" action="." data-abide>
						<?php include(ROOT_PATH . "inc/form.php"); ?>
						<div class="row">
							<div class="small-12 columns text-center">
								<button class="button alert radius" type="submit" name="form-3-submit"><span>Заказать звонок</span><br>Наш менеджер перезвонит вам</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div><!-- end call to action -->
		
		
		<!-- advantages -->
		<div class="padded-container">
			<div id="advantages">
				
				<hr class="top-line">
				
				<div class="row">
					<div class="small-12 columns text-center">
						<h2>Почему выбирают погреба Le Pogreb</h2>
					</div>
				</div>

				<hr class="underline">
				
				<div class="row">
					<table id="comparison">
						<thead>
							<tr>
								<th class="text-center">
									<span>Преимущества</span>
									<p>погребов Le Pogreb:</p>
								</th>
								<th class="text-center">
									<span>Недостатки</span>
									<p>винных шкафов:</p>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-center" colspan="2">
									<h4>Вместимость</h4>
								</td>
							</tr>
							<tr>
								<td class="pros text-center">
									<div class="row">
										<div class="small-12 medium-3 columns medium-left">
											<i class="icon-many-bottles"></i>
										</div>
										<div class="small-12 medium-8 large-9 columns">
											<p>Вы можете выбрать погреб круглой или овальной формы. Вместительность от 800 до 3500 бутылок в зависимости от модели.</p>
										</div>
									</div>
								</td>
								<td class="cons text-center">
									<div class="row">
										<div class="small-12 medium-3 medium-push-9 columns medium-right">
											<i class="icon-three-bottles medium-right"></i>
										</div>
										<div class="small-12 medium-8 large-9 medium-pull-3 columns">
											<p>В одну из самых вместительных моделей винных шкафов Liebherr войдет чуть больше, чем 300 бутылок.</p>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" colspan="2">
									<h4>Температура</h4>
								</td>
							</tr>
							<tr>
								<td class="pros text-center">
									<div class="row">
										<div class="small-12 medium-3 columns medium-left">
											<i class="icon-thermometers"></i>
										</div>
										<div class="small-12 medium-8 large-9 columns">
											<p>Спиральный винный погреб Le Pogreb является мультитемпературным и универсальным для целей хранения вина и подачи его на стол.</p>
										</div>
									</div>
								</td>
								<td class="cons text-center">
									<div class="row">
										<div class="small-12 medium-3 medium-push-9 columns medium-right">
											<i class="icon-thermometer"></i>
										</div>
										<div class="small-12 medium-8 large-9 medium-pull-3 columns">
											<p>Вино в винных шкафах при подаче на стол около +12˚C. Белое и игристое вина нужно охлаждать дополнительно до оптимальной температурЫ, а красное - подогреть.</p>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" colspan="2">
									<h4>Расположение</h4>
								</td>
							</tr>
							<tr>
								<td class="pros text-center">
									<div class="row">
										<div class="small-12 medium-3 columns medium-left">
											<i class="icon-big-house"></i>
										</div>
										<div class="small-12 medium-8 large-9 columns">
											<p>Le Pogreb – инновационная система хранения вина, не требующая дополнительного места в Вашем доме.</p>
										</div>
									</div>
								</td>
								<td class="cons text-center">
									<div class="row">
										<div class="small-12 medium-3 medium-push-9 columns medium-right">
											<i class="icon-house medium-right"></i>
										</div>
										<div class="small-12 medium-8 large-9 medium-pull-3 columns">
											<p>Винный шкаф может не вписываться в интерьер вашего дома.</p>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" colspan="2">
									<h4>Энергопотребление</h4>
								</td>
							</tr>
							<tr>
								<td class="pros text-center">
									<div class="row">
										<div class="small-12 medium-3 columns medium-left">
											<i class="icon-switch"></i>
										</div>
										<div class="small-12 medium-8 large-9 columns">
											<p>Le Pogreb – полностью энергонезависимая система. Температура и влажность достигаются путем установки многоканальной системы вентиляции.</p>
										</div>
									</div>
								</td>
								<td class="cons text-center">
									<div class="row">
										<div class="small-12 medium-3 medium-push-9 columns medium-right">
											<i class="icon-electricity medium-right"></i>
										</div>
										<div class="small-12 medium-8 large-9 medium-pull-3 columns">
											<p>Потребление энергии 24 часа в сутки, 7 дней в неделю, 365 дней в году.</p>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="text-center" colspan="2">
									<h4>Многофункциональность</h4>
								</td>
							</tr>
							<tr>
								<td class="pros text-center">
									<div class="row">
										<div class="small-12 medium-3 columns medium-left">
											<i class="icon-goods"></i>
										</div>
										<div class="small-12 medium-8 large-9 columns">
											<p>По Вашему желанию может быть использован не только под хранение вина, но и сигар, продуктов, книг, а также Вы сможете создать в нем дегустационный зал.</p>
										</div>
									</div>
								</td>
								<td class="cons text-center">
									<div class="row">
										<div class="small-12 medium-3 medium-push-9 columns medium-right">
											<i class="icon-bottle medium-right"></i>
										</div>
										<div class="small-12 medium-8 large-9 medium-pull-3 columns">
											<p>В винных шкафах нельзя хранить ничего, кроме бутылок с вином, даже сигары.</p>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table><!-- end comparison -->
				</div><!-- end row -->
			
				<!-- call to action -->
				<div class="row">
					<div class="medium-7 columns">
						<div class="attention">
							<div class="row">
								<div class="small-2 columns text-center">
									<i class="icon-attention"></i>
								</div>
								<div class="small-10 columns">
									<p>Винный шкаф не вписывается в интерьер, выполняет одну функцию, не может вместить Вашу коллекцию.</p>
									<p>Ваш единственно правильный выбор – Le Pogreb.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="medium-5 columns">
						<div class="form-wrapper">
							<!-- <form name="form-4" method="post" action="<?php echo BASE_URL; ?>" data-abide> -->
							<form name="form-4" method="post" action="." data-abide>
								<?php include(ROOT_PATH . "inc/form.php"); ?>
								<div class="row">
									<div class="small-12 columns text-center">
										<button class="button alert radius" type="submit" name="form-4-submit"><span>Заказать звонок</span><br>Наш менеджер перезвонит вам</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div><!-- end call to action -->
			</div><!-- end advantages -->
		</div><!-- end padded-container -->
		
		
		<!-- installation -->
		<div class="padded-container">
			<div id="installation">

				<hr class="top-line">
				
				<div class="row">
					<div class="small-12 columns text-center">
						<h2>Как мы работаем</h2>
					</div>
				</div>

				<hr class="underline">

				<div class="row">
					<div class="medium-5 columns">
						<p>Принципы построения погребов Le Pogreb:</p>
						<ol>
							<li>Все работы выполняются по индивидуальному чертежу</li>
							<li>Все детали изготавливаются эксклюзивно для Вашего погреба из самых качественных отобранных материалов</li>
							<li>Все наши работы выполняют исключительно профессионально обученные специалисты</li>
							<li>Все Ваши желания по оснащению погреба будут безукоризнено исполнены в самые кратчайшие сроки.</li>
						</ol>
					</div>
					
					<!-- slider -->
					<div class="medium-7 columns">
						<div class="image-slider">
							<div id="slider-img-1"></div>
							<div id="slider-img-2"></div>
							<div id="slider-img-3"></div>
							<div id="slider-img-4"></div>
							<div id="slider-img-5"></div>
							<div id="slider-img-6"></div>
							<div id="slider-img-7"></div>
							<div id="slider-img-8"></div>
						</div>
					</div>
				</div><!-- end slider -->
			</div><!-- end installation -->
		</div><!-- end padded-container -->
		
		<!-- modal success -->
		<div id="success-modal" class="reveal-modal small text-center" data-reveal>
			<h2>Спасибо</h2>
			<p>Наш сотрудник скоро свяжется с вами.</p>
			<a class="close-reveal-modal">&#215;</a>
		</div><!-- end modal success -->
		
		<!-- modal error -->
		<div id="error-modal" class="reveal-modal small text-center" data-reveal>
			<h2>Ошибка!</h2>
			<p>Для связи с нами вы должны ввести:</p>
			<p><?php echo $error_message; ?></p>
			<a class="close-reveal-modal">&#215;</a>
		</div><!-- end modal error -->
		
		<footer>
			<div class="row">
				<div class="medium-2 small-4 columns">
					<div id="logo-img-1"></div>
				</div>
				<div class="medium-2 small-4 columns">
					<div id="logo-img-2"></div>
				</div>
				<div class="medium-2 small-4 columns">
					<div id="logo-img-3"></div>
				</div>
				<div class="medium-2 small-4 columns">
					<div id="logo-img-4"></div>
				</div>
				<div class="medium-2 small-4 columns">
					<div id="logo-img-5"></div>
				</div>
				<div class="medium-2 small-4 columns">
					<div id="logo-img-6"></div>
				</div>
			</div>
			<div class="row">
				<div class="text-center">
					<p>&copy;<?php echo date('Y'); ?></p>
				</div>
			</div>
		</footer>
		
		<script src="js/app-min.js"></script>
		
		<!-- modal reveal -->
		<?php include_once (ROOT_PATH . "inc/reveal.php") ?>
		<!-- <?php include_once (ROOT_PATH . "inc/yandex-metrika.php") ?> -->

	</body>
</html>