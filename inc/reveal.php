<!-- checks if image was sent -->
<?php if ($emailStatus == "sent") {?>
	<!-- show success modal on page load -->
	<script>$(document).ready(function(){$('#success-modal').foundation('reveal', 'open')});</script>

<!-- if there are any error messages -->
<?php ;} elseif (!empty($error_message)) {?>
	<!-- show error modal on page load -->
	<script>$(document).ready(function(){$('#error-modal').foundation('reveal', 'open')});</script>
	
<!-- when form wasn't sent -->
<?php ;} else {?>
	<!-- show no reveal modal -->
	<script>$(document).ready(function(){$('#').foundation('open')});</script>
<?php ;} ?>