<div class="row">
	<div class="small-12 columns">
		<input type="text" name="name" placeholder="Ваше имя*" maxlength="150" value="<?php if(isset($name)) { echo htmlspecialchars($name); } ?>" required>
		<small class="error">Вы должны ввести имя</small>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<input type="text" name="contact" placeholder="Телефон или email*" maxlength="150" value="<?php if(isset($name)) { echo htmlspecialchars($contact); } ?>" required>
		<small class="error">Вы должны оставить свой контакт</small>
	</div>
</div>

<div class="row">
	<div class="small-12 columns">
		<textarea rows="3" name="message" placeholder="Ваш комментарий" maxlength="500"><?php if(isset($name)) { echo htmlspecialchars($message); } ?></textarea>
		<input type="text" name="address" class="address-input" placeholder="Оставьте это поле пустым!">
	</div>
</div>