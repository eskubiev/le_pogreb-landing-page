<?php

//Set empty error message
$error_message = "";

//Set empty email message
$emailStatus = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	//Trim data from whitespaces and assign it to variables
	$name = trim($_POST["name"]);
	//Make fist letter of every word uppercase, all other lowercase
	$name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
	
	$contact = trim($_POST["contact"]);
	
	$message = trim($_POST["message"]);	
	
	
	
	/**************************************
	Check required fields
	**************************************/
	
	//If required field is empty add this info to error message
	if (empty($name)) {
	    $error_message .= "<br/>Имя";
	}
	if (empty($contact)) {
	    $error_message .= "<br/>Свой контакт";
	}
	
	
	
	/**************************************
	Anti-spam
	**************************************/
	
	if (!isset($error_message)) {
	    foreach( $_POST as $value ){
	        if( stripos($value,'Content-Type:') !== FALSE ){
	            $error_message = "С введенной вами информацией возникла проблема.";
	            die();
	        }
	    }
	}
	
	if (!isset($error_message) && $_POST["address"] != "") {
	    $error_message = "В введенной вами информации ошибка";
	    die();
	}
	
	
	
	/**************************************
	Forming and sending email
	**************************************/
	
	//If no errors occured construct the body of the email
	if (empty($error_message)) {
		$emailTo = "skubiev@gmail.com";
		/* $emailTo = "ganymos@gmail.com"; */
		$emailSubject = "Новая заявка";
		$emailMessage = "Имя заявителя: " . $name . "\n" . "Контакт: " . $contact . "\n" . "Время и дата поступления: " . date("H:i d.m.y") . "\n" . "Комментарий: " . $message;
		$emailMessage = wordwrap($emailMessage, 70);
		$emailHeaders = 'Content-type: text/plain; charset = "utf-8"' . "\r\n" .
						'From: ' . "LePogreb <donotreply@lepogreb.ru>" . "\r\n" .
						'X-Mailer: PHP/' . phpversion();
		
		//Send email
		mail($emailTo, $emailSubject, $emailMessage, $emailHeaders);
		
		//Check if email was sent successfully
		if (mail) {
			$emailStatus = "sent";
		}
	}
}

?>