This is Le Pogreb company landing page.

Company specialises on wine cellars installation.

IMPORTANT!
Before placing files on server:

- set correct settings in inc/config.php;
- replace development version of <form> openning tags with commented out ones in index.php;
- uncomment google analytics (inside <head>) and yandex metrika (at the very end right before closing </body>) inclusions in index.php.



Это основная страница компании Le Pogreb.

Компания занимается установкой винных погребов.

ВАЖНО!
Перед размещением проекта на сервере:

- задайте корректные настройки в файле inc/config.php;
- замените рабочие версии открывающих тегов <form> на закомментированные в файле index.php;
- уберите комментирование с включений google analytics (внутри тега <head>) и яндекс метрики (в самом конце, прямо перед закрывающим тегом </body>) в файле index.php.